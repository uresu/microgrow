<?php

class MyDB extends SQLite3
{
    function __construct()
    {
    $this->open(dirname(__FILE__) . '/db/microgrow.db');
    }
}

$days   = $_GET["days"];
$source = $_GET["source"];

$db     = new MyDB();
$sql    = "SELECT * FROM $source WHERE time >= datetime('now','-$days day')";
$result = $db->query($sql);
$data   = array();
if($result == false)
{
    return false;
}
else
{
    while ($res = $result->fetchArray(1))
    {
        array_push($data, $res);
    }
}
echo json_encode($data);