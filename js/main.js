$(document).ready(function()
{
    doTemperatureChart(1, "24");
    doHumidityChart(1, "24");
    doSoilChart(1, "24");

    doTemperatureChart(2, "48");
    doHumidityChart(2, "48");
    doSoilChart(2, "48");

    doTemperatureChart(7, "168");
    doHumidityChart(7, "168");
    doSoilChart(7, "168");

    $( function() {
        $( "#main" ).tabs();
    } );
});

function doTemperatureChart(days, container) {
    var temperatures = [];
    var tempValues = [];
    var heaterValues = [];

    var dataUrl = "/data.php?source=temperature&days=" + days;

    $.getJSON(dataUrl, function (temperaturesData)
    {
        $.each(temperaturesData, function (key, value) 
        {
            temperatures.push({x: new Date(value['time']), y: value['value']});
            tempValues.push(parseFloat(value['value']));
        });

        var heaters = [];

        $.getJSON("/data.php?source=heater&days=" + days, function (heatersData) {
            $.each(heatersData, function (key, value) {
                heaters.push({x: new Date(value['time']), y: value['value']});
                heaterValues.push(parseFloat(value['value']));
            });

            var dataMin       = Math.min.apply(Math, tempValues);
            var dataMax       = Math.max.apply(Math, tempValues);
            var targetElement = container + "-temperatureChartContainer";

            var temperaturesChart = new CanvasJS.Chart(targetElement, {
                title: {
                    text: "Temperature/Heater"
                },
                axisX: {},
                axisY2:
                {
                    title: "",
                    minimum: 0,
                    maximum: 1,
                    tickLength: 0,
                    lineThickness:0,
                    margin:0,
                    valueFormatString:" " //comment this to show numeric values
                },
                axisY:
                {
                    title: "Temperature",
                    minimum: dataMin,
                    maximum: dataMax
                },

                toolTip: {
                    shared: true
                },
                legend: {
                    cursor: "pointer"
                },
                data:
                [
                    {
                    xValueType: "dateTime",
                    type: "spline",
                    name: "Temperature",
                    showInLegend: false,
                    dataPoints: temperatures,
                    axisYIndex: 0,
                    xValueFormatString:"HH:MM",
                },
                    {
                        type: "stepArea",
                        name: "Heater On",
                        showInLegend: false,
                        dataPoints: heaters,
                        axisYIndex: 1,
                        axisYType: "secondary",
                        fillOpacity: .3,
                        color: "red",
                        xValueFormatString:"HH:MM",
                        showInLegend:true
                    }
                ]
            });

            temperaturesChart.render();
        });
    })
}

function doHumidityChart(days, container) {
    var humiditys = [];
    var humidityValues = [];
    var lightValues = [];

    $.getJSON("/data.php?source=humidity&days=" + days, function (humiditysData) {
        $.each(humiditysData, function (key, value) {
            humiditys.push({x: new Date(value['time']), y: value['value']});
            humidityValues.push(parseFloat(value['value']));
        });

        var lights = [];

        $.getJSON("/data.php?source=light&days=1", function (lightsData) {
            $.each(lightsData, function (key, value) {
                lights.push({x: new Date(value['time']), y: value['value']});
                lightValues.push(parseFloat(value['value']));
            });

            var dataMin = Math.min.apply(Math, humidityValues);
            var dataMax = Math.max.apply(Math, humidityValues);
            var targetElement = container + "-humidityChartContainer";

            var humiditysChart = new CanvasJS.Chart(targetElement, {
                title: {
                    text: "Humidity/Light"
                },
                axisX: {},
                axisY2:
                    {
                        title: "",
                        minimum: 0,
                        maximum: 1,
                        tickLength: 0,
                        lineThickness:0,
                        margin:0,
                        valueFormatString:" " //comment this to show numeric values
                    },

                axisY:
                    {
                        title: "Humidity",
                        minimum: dataMin,
                        maximum: dataMax
                    },

                toolTip: {
                    shared: true
                },
                legend: {
                    cursor: "pointer"
                },
                data:
                    [
                        {
                            xValueType: "dateTime",
                            type: "spline",
                            name: "Humidity",
                            showInLegend: false,
                            dataPoints: humiditys,
                            axisYIndex: 0,
                            xValueFormatString:"HH:MM"
                        },
                        {
                            type: "stepArea",
                            name: "Light",
                            showInLegend: false,
                            dataPoints: lights,
                            axisYIndex: 1,
                            axisYType: "secondary",
                            fillOpacity: .4,
                            color: "yellow",
                            xValueFormatString:"HH:MM"
                        }
                    ]
            });

            humiditysChart.render();
        });
    })
}

function doSoilChart(days, container)
{
    var soils = [];

    $.getJSON("/data.php?source=soil&days=" + days, function (soilsData)
    {
        $.each(soilsData, function (key, value) 
        {
            soils.push({x: new Date(value['time']), y: value['value'] * 25});
        });

        var targetElement = container + "-soilChartContainer";
        var uniqueSoil    = container + "soil";
        var soilsChart = new CanvasJS.Chart(targetElement,             
        {
		title: { 
			text: "Soil Hygrometry" 
		}, 
		axisY: { 
			prefix: "%",
            minimum: 0,
            maximum: 100
		}, 
		data: [ 
		{
			type: "stepArea",
            xValueType: "dateTime",
			toolTipContent: "{x}: {y}",
			markerSize: 0, 
			dataPoints: soils,
            xValueFormatString:"HH:MM"
			} 
		] 
        });
        soilsChart.render();
    })
}