import RPi.GPIO as GPIO
import sys

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
pin = int(sys.argv[1])
print "Turning off pin {}".format(pin)
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, GPIO.LOW)
