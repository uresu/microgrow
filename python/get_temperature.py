#!/usr/bin/python

import Adafruit_DHT
import sys
sensor = Adafruit_DHT.DHT22
pin = sys.argv[1]
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin, 1, 1)
if temperature is not None:
    print(temperature)
else:
    print('NULL')
