import RPi.GPIO as GPIO
import sys

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
pin = int(sys.argv[1])
#print "Turning on pin {}".format(pin)
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, GPIO.HIGH)