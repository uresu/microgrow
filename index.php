<!DOCTYPE HTML>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
</head>
<body>
    <div id="main">
        <ul>
            <li><a href="#24hours">24 Hours</a></li>
            <li><a href="#48hours">48 Hours</a></li>
            <li><a href="#7days">7 Days</a></li>
        </ul>
        
        <div id="24hours" >
            <div id="24-temperatureChartContainer" style="height: 370px; width: 100%;"></div>
            <div id="24-humidityChartContainer"    style="height: 370px; width: 100%;"></div>
            <div id="24-soilChartContainer"        style="height: 370px; width: 100%;"></div>
        </div>
        
        <div id="48hours" >
            <div id="48-temperatureChartContainer" style="height: 370px; width: 100%;"></div>
            <div id="48-humidityChartContainer"    style="height: 370px; width: 100%;"></div>
            <div id="48-soilChartContainer"        style="height: 370px; width: 100%;"></div>
        </div>
        
        <div id="7days" >
            <div id="168-temperatureChartContainer"  style="height: 370px; width: 100%;"></div>
            <div id="168-humidityChartContainer"     style="height: 370px; width: 100%;"></div>
            <div id="168-soilChartContainer"         style="height: 370px; width: 100%;"></div>
        </div>
    </div>

</body>
<footer>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script src="js/main.js"></script>
</footer>
</html>