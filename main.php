<?php

//settings
$temperaturePins = array(4, 5, 6, 7);
$humidityPins    = array(4, 5, 6, 7);
$soilPins        = array(8, 9, 10, 11);
$dayMaxTemp      = 25;
$dayMinTemp      = 28;
$nightMaxTemp    = 21;
$nightMinTemp    = 18;

$heater       = new Heater();
$light        = new Light();
$sensors      = new Sensors();
$temperatures = $sensors->getAverageTemperature($temperaturePins);

class Sensors
{
    function getAverageTemperature($temperaturePins)
    {
        $temperatures = array();

        foreach ($temperaturePins as $pin)
        {
            $temperatures[] = $this->getTemperature($pin);
        }

        $temperatures             = array_filter($temperatures);
        $averageTemperature       = array_sum($temperatures)/count($temperatures);
        return $averageTemperature;
    }

    function getTemperature($pin)
    {
        $temp = exec('python ' . dirname(__FILE__) . '/python/get_temperature.py ' . $pin);
        if ($temp != 'NULL')
        {
            return $temp;
        }
        else return false;
    }

    function getAverageHumidity($humidityPins)
    {
        $humidities = array();
        foreach ($humidityPins as $pin)
        {
            $humidities[] = $this->getHumidity($pin);
        }

        $humidities = array_filter($humidities);
        $averageHumidity = array_sum($humidities) / count($humidities);
        return $averageHumidity;
    }

    function getHumidity($pin)
    {
        $humidity = exec('python ' . dirname(__FILE__) . '/python/get_humidity.py ' . $pin);
        if ($humidity != 'NULL')
        {
            return $humidity;
        }
        else return false;
    }

    function getSoils($soilPins)
    {
        $soils = array();

        foreach ($soilPins as $pin)
        {
            $soils[] = $this->getSoil($pin);
        }

        $soils = array_filter($soils);
        $averageSoil = array_sum($soils);
        return $averageSoil;
    }

    function getSoil($pin)
    {
        $temp = exec('python ' . dirname(__FILE__) . '/python/get_soilmoisture.py ' . $pin);
        return $temp;
    }

}

class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open(dirname(__FILE__) . '/db/microgrow.db');
    }

    function log($averageTemperature, $averageHumidity, $averageSoil, $lightStatus, $heaterStatus)
    {
        if ($averageTemperature > 10)
        {
            $sql = 'INSERT INTO `temperature` (`time`, `value`) VALUES (CURRENT_TIMESTAMP,' . $averageTemperature . ');';
            $this->exec($sql);
        }

        if ($averageHumidity < 100)
        {
            $sql = 'INSERT INTO `humidity` (`time`, `value`) VALUES (CURRENT_TIMESTAMP,' . $averageHumidity . ');';
            $this->exec($sql);
        }

        $sql  = 'INSERT INTO `soil` (`time`, `value`) VALUES (CURRENT_TIMESTAMP,'  . $averageSoil . ');';
        $this->exec($sql);

        $sql  = 'INSERT INTO `light` (`time`, `value`) VALUES (CURRENT_TIMESTAMP,'  . $lightStatus . ');';
        $this->exec($sql);

        $sql  = 'INSERT INTO `heater` (`time`, `value`) VALUES (CURRENT_TIMESTAMP,'  . $heaterStatus . ');';
        $this->exec($sql);
    }

}

class Light
{
    function on()
    {
        exec('python ' . dirname(__FILE__) . '/python/relay_on.py 17');
    }

    function off()
    {
        exec('python ' . dirname(__FILE__) . '/python/relay_off.py 17');
    }
}

class Heater
{
    function on()
    {
        exec('python ' . dirname(__FILE__) . '/python/relay_on.py 18');
    }

    function off()
    {
        exec('python ' . dirname(__FILE__) . '/python/relay_off.py 18');
    }
}

$averageTemperature = $sensors->getAverageTemperature($temperaturePins);
$averageHumidity    = $sensors->getAverageHumidity($humidityPins);
$averageSoil        = $sensors->getSoils($soilPins);

$hour = date('H');

if ($hour >= 5)
{
    $light->on();
    $lightStatus = 1;

    if ($averageTemperature < $dayMinTemp)
    {
        $heater->on();
        $heaterStatus = 1;
    }
    else if ($averageTemperature > $dayMaxTemp)
    {
        $heater->off();
        $heaterStatus = 0;
    }
}

if ($hour >= 23)
{
    $light->off();
    $lightStatus = 0;

    if ($averageTemperature < $nightMinTemp)
    {
        $heater->on();
        $heaterStatus = 1;
    }
    else if($averageTemperature > $nightMaxTemp)
    {
        $heater->off();
        $heaterStatus = 0;
    }
}

$db = new MyDB();
$db->log($averageTemperature, $averageHumidity, $averageSoil, $lightStatus, $heaterStatus);